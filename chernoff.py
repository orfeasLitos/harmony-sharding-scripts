# compare precise and convenient Chernoff bounds

import sys
import math
import matplotlib.pyplot as plt
import numpy as np

MIN_DELTA = .25
MAX_DELTA = .5
STEP_DELTA = 0.001
MU = 2**7
#R = 3/4
#N = R*250
#P = 1/4

def precise(delta):
    return ((math.exp(delta))/(1+delta)**(1+delta))**(MU)

def convenient(delta):
    return math.exp(((-delta**2)*MU)/(2+delta))

deltas = np.array([])
precises = np.array([])
convenients = np.array([])

for i in range(int(MIN_DELTA/STEP_DELTA), int(MAX_DELTA/STEP_DELTA)):
    deltas = np.append(deltas, i*STEP_DELTA)
    precises = np.append(precises, precise(deltas[-1]))
    convenients = np.append(convenients, convenient(deltas[-1]))

plt.plot(deltas, precises, color="green")
plt.plot(deltas, convenients, color="red")
plt.show()
