# find how many rounds to wait until enough observers come

from numpy import matmul, array, log2, arange, exp
from scipy.stats import binom
import matplotlib.pyplot as plt
from tqdm import tqdm
from functools import lru_cache
import os
from os.path import exists

def int_if_big(x, cutoff=100):
    return int(x) if x > cutoff else x

@lru_cache(maxsize=1000000)
def pr1(n, i, j):
    return binom.pmf(j-i, 3*n//4-i, (1+i)/(n/4+i))

@lru_cache(maxsize=100000)
def pr_matrix(n, r):
    assert r > 0

    idxes = range(0, 3*n//4+1)
    pr1_matrix = array([
        [pr1(n, i, j) for j in idxes] for i in idxes
    ])

    if r == 1:
        return pr1_matrix
    res = matmul(pr_matrix(n, r//2), pr_matrix(n, r//2))
    if r % 2 == 1:
        res = matmul(res, pr1_matrix)
    return res

def str_pr_matrix(n, r):
    res = ''
    for row in pr_matrix(10, 1):
        res += ', '.join(map("{:8.6f}".format, row)) + '\n'
    return res

def pr(n, r, i, j):
    assert n % 4 == 0

    return pr_matrix(n, r)[i][j]

def secure_r(n, t):
    """
    Returns r such that enough observers will come with high probability,
    i.e. s.t. sum_{j=n/4-1}^{3n/4} pr(n, r, 0, j) >= t
    """
    r = 1
    while sum([pr(n, r, 0, j) for j in range(n//4-1, 3*n//4+1)]) < t:
        r += 1
    return r

xs = []
ys = []
MAX = 3000
the_range = arange(4, MAX//4+1)

if exists("artifacts/observerrounds.txt"):
    with open("artifacts/observerrounds.txt", "r") as f:
        pairs = [list(map(int, line.split(", "))) for line in f.readlines()]
    if len(pairs) < len(the_range):
        for pair in pairs:
            xs.append(pair[0])
            ys.append(pair[1])
        start = len(pairs)
    else:
        for i in range(len(the_range)):
            xs.append(pairs[i][0])
            ys.append(pairs[i][1])
        start = len(the_range)
else:
    start = 0

for i in tqdm(the_range[start:]):
    x = 4*i
    r = secure_r(x, 1-exp(-x**.2))
    xs.append(x)
    ys.append(r)
    with open("artifacts/observerrounds.txt", "a") as f:
        f.write(str((x, r))[1:-1] + "\n")

plt.plot(xs, ys, color = 'r', label = 'secure r')
plt.plot(xs, log2(list(map(int, the_range))), color = 'g', label = 'log(n)')
plt.show()
