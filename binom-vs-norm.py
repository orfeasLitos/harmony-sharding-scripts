from scipy.stats import binom, norm
from math import sqrt

n = 1000
p = 0.4

def f_binom(k):
    return binom.pmf(k, n, p)

def F_norm(x):
    return norm.cdf(x)

def approx(k):
    return F_norm((k-n*p+0.5)/sqrt(n*p*(1-p))) - F_norm((k-n*p-0.5)/sqrt(n*p*(1-p)))

for k in range(n+1):
    print(f_binom(k), approx(k))

# TODO: understand why the two distributions don't match, contrary to expectations set by https://chrispiech.github.io/probabilityForComputerScientists/en/part2/binomial_approx/
