# calculate probability random shuffle is balanced

import sys
import math
import matplotlib.pyplot as plt
from scipy.special import comb
import numpy as np
from decimal import *

DELTA = Decimal(.0012)
MAX_PARTIES = Decimal(2024)
SHARDS = Decimal(2) # SHARDS = n/(c*log(n))

y = np.array([])

def subsums(n, idxs_sum, low, high, shard):
    if shard == 1:
        return 0
    res = 0
    for i in np.arange(low, high):
        combinations = comb(int(n-idxs_sum), i, exact = True)
        internal_sums = subsums(n, idxs_sum+i, low, high, shard-1)
        res += combinations * internal_sums
    return res

# TODO: use subsums
for n in np.arange(Decimal(1), MAX_PARTIES):
    low = math.ceil((Decimal(1) - DELTA) * n / SHARDS)
    high = math.floor((Decimal(1) + DELTA) * n / SHARDS) + Decimal(1)
    rng = np.arange(low, high)
    val = sum([comb(int(n), int(i), exact = True) for i in rng])
    numer = Decimal(val)
    expon = 2**n
    denom = Decimal(expon)
    val = numer / denom
    y = np.append(y, val)

plt.plot(np.arange(Decimal(1), MAX_PARTIES), y)
plt.show()
