# find Chernoff delta and shuffle rate r
# such that the bad event is unlikely

from math import exp

GOAL = .95
CUTOFF = 0.0001

def f(d):
    return (exp(d)/((1+d)**(1+d)))**((2**9)/(3*(3-d)))

def r(d):
    return 8/(3*(3-d))

d_low = 0
d_hi = .1

while abs(d_low-d_hi) > CUTOFF:
    d_mid = (d_low+d_hi)/2
    if f(d_mid) > GOAL:
        d_low = d_mid
    else:
        d_hi = d_mid

print("delta:", d_low)
print("prob. succ.:", f(d_low))
print("r:", r(d_low))
