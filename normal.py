# find probability of failure with normal approximation

from math import pi, sqrt
from scipy.stats import norm
import matplotlib.pyplot as plt
import numpy as np

R = 1
N = 1000
C = 4

print(1-norm.cdf(0, loc=-(R*N)/(12*C), scale =
    sqrt(
        (1-1/C)*(5*R*N)/4)
    )
)
