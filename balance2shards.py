# calculate probability 2-shard random shuffle is balanced

import sys
import math
import matplotlib.pyplot as plt
from scipy.special import comb
import numpy as np
from decimal import *

DELTA = Decimal(.0012)
MAX_PARTIES = Decimal(2024)

y = np.array([])

for n in np.arange(Decimal(1), MAX_PARTIES):
    low = math.ceil((Decimal(1) - DELTA) * n / Decimal(2))
    high = math.floor((Decimal(1) + DELTA) * n / Decimal(2)) + Decimal(1)
    rng = np.arange(low, high)
    val = sum([comb(int(n), int(i), exact = True) for i in rng])
    numer = Decimal(val)
    expon = 2**n
    denom = Decimal(expon)
    val = numer / denom
    y = np.append(y, val)

plt.plot(np.arange(Decimal(1), MAX_PARTIES), y)
plt.show()
