from scipy.stats import binom, norm
from math import sqrt
from time import perf_counter
import matplotlib.pyplot as plt

p = .5
q = 1-p
BASE_N = 10000
RANGE = 50

#binomial = 0
#
#for n in range(BASE_N, BASE_N+RANGE):
#    for k in range(n//3, 2*n//3):
#        tic = perf_counter()
#        binom.pmf(k, n, p)
#        tac = perf_counter()
#        binomial += tac - tic
#print("binomial:", binomial)
#
#normal = 0
#
#for n in range(BASE_N, BASE_N+RANGE):
#    for k in range(n//3, 2*n//3):
#        n_norm = n*p
#        sigma = n_norm*q
#        tic = perf_counter()
#        norm.pdf(k, n_norm, sigma)
#        tac = perf_counter()
#        normal += tac - tic
#print("normal:", normal)
#
#cumu_normal = 0
#
#for n in range(BASE_N, BASE_N+RANGE):
#    for k in range(n//3, 2*n//3):
#        n_norm = n*p
#        sigma = n_norm*q
#        tic = perf_counter()
#        norm.cdf(k+.5, n_norm, sigma) - norm.cdf(k-.5, n_norm, sigma)
#        tac = perf_counter()
#        cumu_normal += tac - tic
#print("cumulative normal:", cumu_normal)

xs = []
norms = []
binoms = []
n_norm = BASE_N*p
sigma = sqrt(n_norm*q)
offset = .0001 # to avoid complete overlap
for k in range(BASE_N):
    xs.append(k)
    norms.append(offset+norm.cdf(k+.5, n_norm, sigma) - norm.cdf(k-.5, n_norm, sigma))
    binoms.append(binom.pmf(k, BASE_N, p))

plt.plot(xs, norms, color = 'r', label = 'normal approx')
plt.plot(xs, binoms, color = 'g', label = 'binom exact')
plt.show()
