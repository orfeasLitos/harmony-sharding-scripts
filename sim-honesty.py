# compare permutation-based and independent shuffling

from random import random, randrange
from math import floor

R = 1

# Harmony
H = 751
M = 249
C = 4
MAX_BAD_RATIO = 1/3

# OmniLedger
#H = 1351
#M = 449
#C = 3
#MAX_BAD_RATIO = 1/3

# RapidChain
#H = 2667
#M = 1333
#C = 16
#MAX_BAD_RATIO = 0/3

def bias_to_old():
    fails = 0
    reps = 0
    #TODO: continue

    for i in range(C):
        if assigned_bad[i]/(assigned_good[i]+assigned_bad[i]) >= MAX_BAD_RATIO:
            fails += 1
            break

    reps += 1
    if reps % 100 == 0:
        print(fails, fails/reps)

# permutes everyone
def permutation():
    fails = 0
    reps = 0
    full_shard = (H+M) // C
    assert float(full_shard) == (H+M) / C

    while True:
        assigned_good = [0] * C
        assigned_bad = [0] * C
        allocated = 0
        pending_good = H
        pending_bad = M

        while pending_good + pending_bad > 0:
            if random() < pending_bad / (pending_good + pending_bad):
                assigned_bad[floor(allocated/full_shard)] += 1
                pending_bad -= 1
                assert pending_bad >= 0
            else:
                assigned_good[floor(allocated/full_shard)] += 1
                pending_good -= 1
                assert pending_good >= 0
            allocated += 1

        for i in range(C):
            if assigned_bad[i]/(assigned_good[i]+assigned_bad[i]) >= MAX_BAD_RATIO:
                fails += 1
                break

        reps += 1
        if reps % 100 == 0:
            print(fails, fails/reps)

def independent():
    fails = 0
    reps = 0

    while True:
        assigned_good = [0] * C
        assigned_bad = [0] * C

        for _ in range(H):
            if random() <= R: # move R*H honest parties
                assigned_good[randrange(C)] += 1
        for _ in range(M):
            if random() <= R: # move R*M corrupt parties
                assigned_bad[randrange(C)] += 1
            else:
                assigned_bad[0] += 1

        for i in range(C):
            if assigned_bad[i]/(assigned_good[i]+assigned_bad[i]) >= MAX_BAD_RATIO:
                fails += 1
                break

        reps += 1
        if reps % 100 == 0:
            print(fails, fails/reps)

try:
    #independent()
    #bias_to_old()
    permutation()
except KeyboardInterrupt:
    pass
