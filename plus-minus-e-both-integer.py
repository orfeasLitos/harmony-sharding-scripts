# test whether nudged and scaled 1 is consistently (non-)integer

for i in range(1000):
    for j in range(1000):
        e = .001*j
        low = round((1-e)*i,8)
        high = round((1+e)*i,8)
        if low.is_integer() and not high.is_integer():
            raise ValueError(low, high)
        if not low.is_integer() and high.is_integer():
            raise ValueError(low, high)
