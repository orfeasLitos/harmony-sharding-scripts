# https://www.statology.org/normal-approximation/

from scipy.stats import norm, binom
from math import sqrt, isclose
import matplotlib.pyplot as plt

n = 100
X = 43
X_hi = X+.5
X_lo = X-.5
p = .5
q = 1 - p

m = n*p
assert isclose(m, 50)

s = sqrt(m*q)
assert isclose(s, 5)

z = (X_hi-m)/s
assert isclose(z, -1.3)

assert isclose(norm.cdf(X_hi, m, s), norm.cdf(z))
assert isclose(binom.cdf(X, n, p), norm.cdf(z), rel_tol=.01)

assert isclose(norm.cdf(X_hi, m, s) - norm.cdf(X_lo, m, s), binom.pmf(X, n, p), rel_tol=.01)

xs = []
norms = []
binoms = []
for k in range(n):
    xs.append(k)
    norms.append(0.1+norm.cdf(k+.5, m, s) - norm.cdf(k-.5, m, s))
    binoms.append(binom.pmf(k, n, p))

plt.plot(xs, norms, color = 'r', label = 'normal approx')
plt.plot(xs, binoms, color = 'g', label = 'binom exact')
plt.show()
