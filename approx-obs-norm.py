# find range where normal approximates binomial

from math import sqrt, floor, ceil, inf

def admissible_range1(n):
    D = 36-11*n+9*n**2/16
    minus_b = 3*n/4 - 6
    double_a = 2
    i1 = (minus_b - sqrt(D))/double_a
    i2 = (minus_b + sqrt(D))/double_a
    return ceil(i1), floor(i2)

def admissible_range2(n):
    return -inf, floor((3*n**2/16-2*n)/(n/4+4))

def common_range(pair1, pair2):
    low = max(pair1[0], pair2[0])
    hi = min(pair1[1], pair2[1])
    return low, hi if low <= hi else None

def admissible_range(n):
    return common_range(admissible_range1(n), admissible_range2(n))

for n in range(16, 1000):
    n_range = admissible_range(n)
    if n_range is None:
        print(n, "No admissible range")
    else:
        print(n, n_range)

# Conclusions
# - the range is comfortably big for approximation
# - the upper limit is decided by admissible_range2
