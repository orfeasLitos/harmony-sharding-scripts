# find probability that no observers come after n rounds

def prob_no_observers(n, r):
    return ((1-4/n)**(3*n/4))**r

n = 4
rounds = 3

while True:
    print(prob_no_observers(n, rounds))
    n += 1
